
# coding: utf-8

# In[ ]:


class domain:
    def __init__(self, pathinput):
        with open(pathinput + '/bdata.txt', 'r') as f:
            bda = f.readline().split(',')
            # bda=map(float, bda[:-1])
        with open(pathinput + '/mnums.txt', 'r') as f:
            self.nums = f.read().splitlines()
            self.ends0, self.evals, self.modes = map(int, self.nums[: 3])
            self.lengths0 = map(float, self.nums[-2:])
            self.ends = int(bda[0])
        self.lengths = map(float, bda[1+4*self.ends: -1])
        with open(pathinput + '/ppmcalcsize.txt', 'r') as f:
            bda = f.read().splitlines()
            self.modes1, self.pp, self.ends1 = int(bda[0]), float(bda[1]), int(bda[2])

        if (self.ends0 == self.ends and self.ends == self.ends1):
            print(self.ends, 'ends ok')
        else:
            print('ends mismatch, something is wrong')

        if (self.lengths == self.lengths0):
            print(self.lengths, 'lengths ok')
        else:
            print('lengths mismatch, something is wrong')

        if (self.modes == self.modes1):
            print(self.modes, 'modes available, all ok')
        else:
            print('modes mismatch, something is wrong')

        print(self.evals, " available")

        with open(pathinput+'/ppmndfcs.txt', 'r') as f:
            self.ndm04 = map(float, f.read().splitlines())
            self.ndm04 = column_matrix(RDF, self.modes*self.ends, self.ndm04)

        with open(pathinput+'/meval.txt', 'r') as f:
            self.mus = map(float, f.read().splitlines())

        with open(pathinput+'/mevec.txt', 'r') as f:
            self.mevec = map(float, f.read().splitlines())
            self.mevec = matrix(CDF, self.evals, self.mevec)
