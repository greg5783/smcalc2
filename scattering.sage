
# coding: utf-8

# In[ ]:


class scattering:
    def __init__(self, domaininput, zm, rm, rev):
        # need to throw errors here if the parameters are stupid
        # the evals and evecs
        int(rev)
        int(rm)
        self.pp = domaininput.pp
        self.rmus = domaininput.mus[: rev]
        self.rmodes = rm
        self.revals = rev
        modetrimmer = flatten(
            [map(lambda m: m+i*domaininput.modes,
                 range(rm)) for i in range(domaininput.ends)])
        self.rndm04 = domaininput.ndm04.matrix_from_rows_and_columns(modetrimmer, modetrimmer)
        self.rmevec = domaininput.mevec.submatrix(row=0, nrows=rev)
        self.rmevec = self.rmevec.matrix_from_columns(modetrimmer)
        self.rmevecT = self.rmevec.T
        self.rzeros = [0 for i in range(self.rndm04.nrows())]

        self.zeromodes = zm

        self.zeropoz = []
        self.ends = domaininput.ends
        test0 = [0 for i in range(self.rmodes*domaininput.ends)]
        test0col = [0 for i in range(self.revals)]
        test0 = [0 for i in range(self.rmodes*domaininput.ends)]
        test0col = [0 for i in range(self.revals)]
        self.r0mevec = deepcopy(self.rmevec)
        self.r0ndm04 = deepcopy(self.rndm04)
        for i in range(0, domaininput.ends):
            for j in zm[i]:
                self.r0mevec.set_column((i*self.rmodes)+j, test0col)
                self.r0ndm04.set_row((i*self.rmodes)+j, test0)
                self.zeropoz.append((i*self.rmodes)+j)
        self.kernelpoz = [self.rmodes*domaininput.ends-i-1
                          for i in range(len(self.zeropoz))]
        self.smdim = len(flatten(self.zeromodes))
        self.kernelpoz.reverse()
        self.dkernelpoz = map(lambda x: x+self.rmodes*domaininput.ends, self.kernelpoz)
        self.dzeropoz = map(lambda x: x+self.rmodes*domaininput.ends, self.zeropoz)
        self.mmlengths = [
            j for j in domaininput.lengths for i in range(0, self.rmodes)]
        self.mmevs = [
            i for j in domaininput.lengths for i in range(0, self.rmodes)]
        self.DZEROV = zero_vector(2*domaininput.ends * self.rmodes)
        self.ZEROV = zero_vector(domaininput.ends * self.rmodes)

        self.mmsubtraction = (
            pi * np.array(self.mmevs) / np.array(self.mmlengths)) ^ 2
        self.drndmcomponents = np.array(self.rmus)

    # bottom two methods are souirce of most of
    # the processing time for dsm and need to be sped up

    def rndm(self, l):
        def components(x):
            return (self.pp - l) / (l * self.pp - l * x - self.pp * x + x ^ 2)
        co = diagonal_matrix(CDF, map(components, self.rmus), sparse=False)
        return self.rndm04-(self.rmevecT) * co * self.rmevec

    def drndm(self, l):
        def components(x):
            return 1/((l - x) ^ 2)
        co = diagonal_matrix(CDF, map(components, self.rmus), sparse=False)
        return (self.rmevecT) * co * self.rmevec

    # A new version of rmm which is slightly faster

    def rmm(self, l):
        return diagonal_matrix(
            CDF, 1/(I*sqrt(l - (self.mmsubtraction))), sparse=False)

    def drmm(self, l):
        return diagonal_matrix(CDF,
                               -I*sqrt(l-self.mmsubtraction), sparse=False)

    # BIG weakness here need to adapt to differetn lengths

    def diagdiff(self, l):
        def f(y):
            return I*sqrt(l - (y * pi/2) ^ 2)
        if imaginary(l) < 0:
            return -diagonal_matrix(CDF,
                                    map(f, flatten(self.zeromodes)),
                                    sparse=False)
        else:
            return diagonal_matrix(CDF, map(f, flatten(self.zeromodes)),
                                   sparse=False)

    # this returns the scattering matrix
    def sm(self, l):
        srndm = self.rndm(l)
        srmm = self.rmm(l)
        dd = self.diagdiff(l)
        rxxxs = srndm-srmm
        for i in self.zeropoz:
            rxxxs.set_row(i, self.ZEROV)

        W = (rxxxs.SVD()[2]).matrix_from_columns(self.kernelpoz)

        basis1 = W.matrix_from_rows(self.zeropoz)
        basis2 = (srndm*W).matrix_from_rows(self.zeropoz).inverse()
        psm = basis1*basis2
        return (psm-dd).inverse()*(psm+dd)

    # this returns the scattering matrix: dsm(l)[0] and its first derivative
    # dsm(l)[1] given the number of modes, modes, and evalues supplied
    # at present 5 loops, best of 3: 2.65 s per loop,
    # 1.4+I 20 modes 2000 eigenvalues
    # 1.87 by setting sparse to false during construction of diagonal matrices
    def dsm(self, l):

        zb = matrix(self.rmodes*self.ends)

        irndm = self.rndm(l)
        idrndm = self.drndm(l)
        idintsystem = irndm.augment(zb).stack((idrndm).
                                              augment(irndm))

        irmm = self.rmm(l)
        idrmm = self.drmm(l)
        idrmmsys = (-idrmm.augment(zb).
                    stack((0.5*irmm).augment(idrmm))).inverse()

        svdoutput = idintsystem-idrmmsys
        for i in self.zeropoz:
            svdoutput.set_row(i, self.DZEROV)

        dd = self.diagdiff(l)
        ddtwo = -0.5*dd.inverse()
        W = (svdoutput.SVD()[2]).matrix_from_columns(self.dkernelpoz)
        A = [W.matrix_from_rows(self.zeropoz),
             W.matrix_from_rows(self.dzeropoz)]
        WB = idintsystem*W
        B = [WB.matrix_from_rows(self.zeropoz).inverse(),
             WB.matrix_from_rows(self.dzeropoz).inverse()]
        P = [A[0]*B[0], A[1]*B[1]]
        S = (P[0]-dd).inverse()*(dd+P[0])
        D = (P[1]-dd).inverse()*(ddtwo*S+ddtwo)
        return [S, D]
